import { useState, useEffect } from 'react';
import schema from './data/schema.json';
import { objectGenerator } from './utils/schemaParser';
import './App.css';

const App = () => {
  const [jsonSchema, setjsonSchema] = useState();
  const [resultingObject, setResultingObject] = useState(JSON.stringify({}));

  useEffect(() => {
    setjsonSchema(JSON.stringify(schema, undefined, 2));
  }, [])

  const handleTextareaChange = ev => {
    ev.preventDefault();
    setjsonSchema(ev.target.value);
  }

  const generateObject = ev => {
    ev.preventDefault();
    setResultingObject(JSON.stringify(objectGenerator(JSON.parse(jsonSchema)), undefined, 2));
  }

  return (
    <div className="App">
      <header className="App-header">
        <p>
          JSON schema fake object generator
        </p>
        <a
          className="App-link"
          href="https://gitlab.com/serDavydko/json-schema-to-object-generator"
          target="_blank"
          rel="noopener noreferrer"
        >
          Source code
        </a>
      </header>
      <section className="App-body">
        <div className="right">
          <h3>Schema:</h3>
            <textarea name="json" onChange={ev => handleTextareaChange(ev)} value={jsonSchema} />
        </div>
        <div className="left">
          <h3>Object:</h3>
          <textarea name="resultingObject" readOnly value={resultingObject} />
          <button onClick={generateObject}>Generate</button>
        </div>
      </section>
    </div>
  );
}

export default App;
