import RandExp from 'randexp';

const LIPSUM_WORDS = `Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore
et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
pariatur Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est
laborum`.split(/\W/);

const config = {
    'MAX_WORDS': 7,
    'MAX_REGEXPR': 20,
    'MIN_NUMBER': -10000,
    'MAX_NUMBER': 10000,
    'MAX_ARR_ELEMS': 7
}

const enumGenerator = (array) => {
    if (Array.isArray(array)) {
        return array[number(0, array.length - 1)];
    }
    return null;
}

const booleanGenerator = () => Math.random() < 0.5;
const nullGenerator = () => null;
const stringGenerator = (obj) => {
    if (obj.pattern) {
        return randexp(obj.pattern);
    }
    return thunkGenerator(obj.minLength, obj.maxLength);
}

const numberGenerator = (value) => {
    let min = typeof value.minimum === 'undefined' ? config.MIN_NUMBER : value.minimum;
    let max = typeof value.maximum === 'undefined' ? config.MAX_NUMBER : value.maximum;

    const multipleOf = value.multipleOf;

    if (multipleOf) {
        max = Math.floor(max / multipleOf) * multipleOf;
        min = Math.ceil(min / multipleOf) * multipleOf;
    }

    if (value.exclusiveMinimum && min === value.minimum) {
        min += multipleOf || 1;
    }

    if (value.exclusiveMaximum && max === value.maximum) {
        max -= multipleOf || 1;
    }

    if (multipleOf) {
        if (String(multipleOf).includes('.')) {
            let base = number(Math.floor(min / multipleOf), Math.floor(max / multipleOf)) * multipleOf;
            while (base < min) {
                base += value.multipleOf;
            }

            return base;
        }

        const boundary = (max - min) / multipleOf;

        let num;
        let fix;

        do {
            num = number(0, boundary) * multipleOf;
            fix = (num / multipleOf) % 1;
        } while (fix !== 0);

        return min + num;
    }

    return number(min, max, true);
}

const integerGenerator = (obj) => {
    return numberGenerator(Object.assign({ multipleOf: 1 }, obj));
}

function randexp(value) {
    RandExp.prototype.max = config.MAX_REGEXPR;
    const re = new RandExp(value);

    return re.gen();
}

function thunkGenerator(min = 0, max = 140) {
    const _min = Math.max(0, min);
    const _max = number(_min, max);

    let result = produce();

    while (result.length < _min) {
        result += produce();
    }

    if (result.length > _max) {
        result = result.substr(0, _max);
    }

    return result;
}

function number(min, max, hasPrecision = false) {
    if (max < min) {
        max += min;
    }

    if (hasPrecision) {
        return getRandom(min, max);
    }

    return getRandomIntegerInclusive(min, max);
}

function getRandom(min, max) {
    return Math.random() * (max - min) + min;
}

function getRandomIntegerInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function produce() {
    const length = number(1, 7);

    return wordsGenerator(length).join(' ');
}

function wordsGenerator(length) {
    const words = shuffle(LIPSUM_WORDS);

    return words.slice(0, length);
}

function shuffle(collection) {
    let tmp;
    let key;
    let length = collection.length;

    const copy = collection.slice();

    for (; length > 0; length--) {
        key = Math.floor(Math.random() * length);
        tmp = copy[length];
        copy[length] = copy[key];
        copy[key] = tmp;
    }

    return copy;
}

export { config, booleanGenerator, nullGenerator, stringGenerator, 
    numberGenerator, integerGenerator, enumGenerator, number 
};
