import {
    booleanGenerator, nullGenerator, stringGenerator,
    numberGenerator, integerGenerator, enumGenerator, number, config
} from './generators';

const SCALAR_GENERATORS = {
    'string': stringGenerator,
    'number': numberGenerator,
    'integer': integerGenerator,
    'null': nullGenerator,
    'boolean': booleanGenerator
};

const ALL_GENERATORS = {
    ...SCALAR_GENERATORS,
    'enum': enumGenerator,
    'object': () => { },
    'array': () => []
}

function isScalar(obj) {
    return SCALAR_GENERATORS.hasOwnProperty(obj.type);
}

function uniqueArrayOfObjects(a) {
    return [...new Set(a.map(o => JSON.stringify(o)))].map(s => JSON.parse(s))
};

function instantiatePrimitive(obj) {
    if (obj.hasOwnProperty('default')) {
        return obj.default;
    };

    return ALL_GENERATORS[obj.type](obj);
}

function instantiateEnum(obj) {
    if (obj.hasOwnProperty('default')) {
        return obj.default;
    };

    return ALL_GENERATORS['enum'](obj.enum);
}

function isEnum(obj) {
    return obj.hasOwnProperty('enum');
}

function findDefinition(schema, ref) {
    let key = ''
    if (ref.startsWith('#')) {
        key = ref.substring(1);
    }
    let definitions = schema.definitions || {};
    return definitions[key] || {}
}

function objectGenerator(schema = {}) {
    let resulting = {};

    function visitKey(obj, name, data) {

        if (obj.type === 'object' && obj.properties) {
            data[name] = data[name] || {};

            for (let property in obj.properties) {
                visitKey(obj.properties[property], property, data[name])
            }
        } else if (obj.type === 'array' && obj.items) {
            data[name] = instantiatePrimitive(obj);
            const min = obj.minItems || 0;
            const max = obj.maxItems ? obj.maxItems : config.MAX_ARR_ELEMS;
            let len = number(min, max);

            for (let i = 0; i < len; i++) {
                visitKey(obj.items, i, data[name]);
            }
            if (obj.uniqueItems) {
                data[name] = uniqueArrayOfObjects(data[name]);
            }
        } else if (obj.$ref) {
            let definition = findDefinition(schema, obj.$ref);
            data[name] = data[name] || {};
            visitKey(definition, name, data);
        } else if (obj.anyOf) {
            data[name] = data[name] || {};
            visitKey(obj.anyOf[number(0, obj.anyOf.length - 1)], name, data);
        } else if (isEnum(obj)) {
            data[name] = instantiateEnum(obj);
        } else if (isScalar(obj)) {
            data[name] = instantiatePrimitive(obj);
        }
    }

    visitKey(schema, 'main', resulting);

    return resulting['main'];
}

export { objectGenerator };
